import re 
  
def Find(string): 
    # findall() 查找匹配正则表达式的字符串
    url = re.findall('https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+', string)
    return url 
      
 
string = '望曦开源团队 的官网地址为: https://www.wangxios.com, Google 的网页地址为: https://www.google.com'
print("Urls: ", Find(string))