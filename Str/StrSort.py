# 字符串按key排序
def OfKey():  
  
    # 声明字典
    key_value ={}     
 
    # 初始化
    key_value[2] = 56       
    key_value[1] = 2 
    key_value[5] = 12 
    key_value[4] = 24
    key_value[6] = 18      
    key_value[3] = 323 
 
    print ("按键(key)排序:")   
 
    # sorted(key_value) 返回重新排序的列表
    # 字典按键排序
    for i in sorted (key_value) : 
        print ((i, key_value[i]), end =" ") 

def OfValue():  
 
    # 声明字典
    key_value ={}     
 
    # 初始化
    key_value[2] = 56       
    key_value[1] = 2 
    key_value[5] = 12 
    key_value[4] = 24
    key_value[6] = 18      
    key_value[3] = 323 

    print ("\n按值(value)排序:")   
    print(sorted(key_value.items(), key = lambda kv:(kv[1], kv[0]))) 

def main(): 
    # 按key排序
    OfKey()  
    # 按value排序
    OfValue()            
      
# 主函数
if __name__=="__main__":      
    main()