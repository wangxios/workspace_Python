# workspace_Python
Python案例集锦

### [python基础](./base)

1. [HelloWorld](./base/HelloWorld.py)
2. [两个数字求和](./base/Sum.py)
3. [开平方根](./base/Sqrt.py)
4. [生成随机数](./base/Randint.py)
5. [斐波那契数列](./base/Fibonacci.py)
6. [阿姆斯特朗数](./base/Armstrong.py)
7. [进制转换](./base/Scale.py)
8. [ASCII码转换](./base/ASCII.py)
9. [简易计算器](./base/Calc.py)
10. [日历表](./base/Calendar.py)
11. [摄氏度转华氏度](./base/Celsius.py)
12. [阶乘](./base/Factorial.py)
13. [进制转换](./base/Binary.py)
14. [文件读写](./base/io.py)
15. [字符串大小写转换](./base/Upper.py)
16. [约瑟夫生死算法](./base/Joseph.py)
17. [五人分鱼](./base/FiveFish.py)
18. [秒表](./base/StopWatch.py)
19. [计算多个自然数的立方和](./base/SumOfSeries.py)
20. [获取昨天的日期](./base/YesterDay.py)

### [List列表](./List/)

1. [列表元素之和](./List/Sum.py)
2. [数组翻转指定个数的元素-1](./List/LeftRotate-1.py)
3. [数组翻转指定个数的元素-2](./List/LeftRotate-2.py)
4. [数组翻转指定个数的元素-3](./List/LeftRotate-3.py)
5. [列表翻转指定位置的元素-1](./List/SwapPositions-1.py)
6. [列表翻转指定位置的元素-2](./List/SwapPositions-2.py)
7. [列表翻转指定位置的元素-3](./List/SwapPositions-3.py)
8. [判断元素是否存在列表中](./List/isExist.py)
9. [列表去重](./List/ListUniqe.py)
10. [列表复制](./List/ListClone.py)
11. [计算元素在列表中出现的次数](./List/CountX.py)


### [Str字符串](./Str)

1. [移除指定位置的字符串](./Str/removeElement.py)
2. [判断字符串是否存在子字符串](./Str/isSubStr.py)
3. [正则表达式提取网址](./Str/ExtractUrl.py)
4. [将字符串作为代码执行](./Str/ExecStr.py)
5. [字符串切片](./Str/StrRotate.py)
6. [字符串对字典进行排序](./Str/StrSort.py)
7. [移除字典键值对](./Str/RemoveKeyValue.py)
8. [合并两个字典](./Str/MergaDict.py)
9. [将字符串的时间转换为时间戳](./Str/StrpTime.py)
10. [将时间戳转换为指定格式的日期](./Str/StrfTime.py)

### [排序与查找算法](./SortAndFind)

1. [二分查找](./SortAndFind/BinarySearch.py)
2. [线性查找](./SortAndFind/LinearSearch.py)
3. [插入排序](./SortAndFind/InsertionSoft.py)
4. [快速排序](./SortAndFind/QuickSort.py)
5. [选择排序](./SortAndFind/SelectionSort.py)
6. [冒泡排序](./SortAndFind/BubbleSort.py)
7. [归并排序](./SortAndFind/MergaSort.py)
8. [堆排序](./SortAndFind/HeapSort.py)
9. [计数排序](./SortAndFind/CountSort.py)
10. [希尔排序](./SortAndFind/ShellSort.py)
11. [拓扑排序](./SortAndFind/CollectionSort.py)

### [小案例](./Project/)

1. [小型银行系统](./Project/bank/)
2. [Socket通讯](./Project/Socket/)
3. [STMP发送邮件](./Project/STMP/)
4. [火车站多线程买票](./Project/BuyTickets/)


### [NumPy](./NumPy)

1. [生成对角矩阵](./NumPy/src/DiagonalMatrix.py)

### [Matplotlib](./Matplotlib)

1. [axes](./Matplotlib/src/axes.py)

### [桌面客户端](./graphiacl)

1. [HelloWorld](./graphiacl/src/HelloWorld/HelloWorld.py)

### [三维建模](./VPython)

1. [球体](./VPython/src/ball.py)

### [Turtle绘图](./Turtle/src/)

1. [四叶草](./Turtle/src/Clover.py)
