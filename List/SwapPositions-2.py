'''
定义一个列表，并将列表中的指定位置的两个元素对调。
例如，对调第一个和第三个元素：

对调前 : List = [23, 65, 19, 90], pos1 = 1, pos2 = 3
对调后 : [19, 65, 23, 90]
'''

def swapPositions(list, pos1, pos2):
     
    first_ele = list.pop(pos1)    
    second_ele = list.pop(pos2-1)
     
    list.insert(pos1, second_ele)  
    list.insert(pos2, first_ele)  
     
    return list
 
List = [23, 65, 19, 90]
pos1, pos2  = 1, 3
 
print(swapPositions(List, pos1-1, pos2-1))