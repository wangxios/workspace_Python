# 从列表中删除重复的元素
list_1 = [1, 2, 1, 4, 6]

print(list(set(list_1)))

# 删除两个列表中重复的元素 合并成一个数组
list_2 = [7, 8, 2, 1]

print(list(set(list_1) ^ set(list_2)))