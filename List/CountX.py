# 定义一个列表，并计算某个元素在列表中出现的次数

lst = [8, 6, 8, 10, 8, 20, 10, 8, 8]
x = 8

def countX(lst, x):
    count = 0
    for ele in lst:
        if (ele == x):
            count = count + 1
    return count

def countX2(lst, x):
    return lst.count(x)

print(countX(lst, x))
print(countX2(lst, x))